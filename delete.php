<?php
require("config.php");
$filename = $_GET['filename']; 
if( ! is_file($filename) || $filename[0] == '.' || $filename[0] == '/' || (substr($filename,-4) != ".kml")){
	die("Bad access attempt.\n");	
}
unlink($filename);
echo $analyticsCode;
?>
All done - Your data was deleted from the Internet!

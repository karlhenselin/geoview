<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" /><title>GeoView Mapping Software by Karl Henselin</title>
	<link rel="stylesheet" type="text/css" href="geostyle2.css">
</head>
<body class="shaded center">
<h1>GeoView turns spreadsheets into beautiful maps!</h1>

<div class="y400">
<img src="Geoview%20turns%20Spreadsheets%20into%20beautiful%20Google%20Earth%20maps.jpg" class="banner" alt="Geoview mapping software turns spreadsheets into beautiful Google Earth maps."/>
</div>
<h3>Your CSV file should have a header. like this - Name, Street Address, City, State, Zipcode, [color-code / year / subset]</h3><p><a href="sample.csv">Download a sample CSV file to modify.</a>
<br> You can put any label on the 6th column and use any kinds of values you like, to color your data with. It is fine to have missing data in any column too.
<br>The color-code is optional. It gives a picture of where your data is coming from or moving towards.
<br>
<h3>You need to install a KML viewer like <a href="https://www.google.com/earth/download/ge/agree.html">Google Earth</a> to view the file you download.</h3>
	<form class="coolform" enctype="multipart/form-data" action="geoview.php" method="POST">
	<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
	Choose a file to upload: <input name="uploadedfile" type="file" /><br />
	Choose an output filename (no extension needed): <input name="fileout" type="text" /><br />
	<input type="submit" value="Upload File" onclick="document.body.innerHTML = '<h2>Please wait. This might take a while...</h2><h3>If the Internet is <i>spinning</i>, things are working...</h3>';"/>
	<br>When you have uploaded the file, it will process, which takes a while if you have several hundred+ in your list.<br>Then, there will be buttons for download and delete.
	<br>Download first, then delete.
	</form><br><br>
Geoview is an open-source project. View or copy the source on <a href="https://bitbucket.org/karlhenselin/geoview/src">BitBucket</a>.
<?php
	require("config.php");
	echo $analyticsCode;
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</body></html>
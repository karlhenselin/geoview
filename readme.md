# README #

Geoview is a complete, hosted webapp at http://old.stmls.org/geoview/

The source is here in case company policy or privacy concerns would ask that you make a local copy of the app or you want to see the source, you want to tweak it, you need to use a paid API key to do more maps faster, or whatever.

### What is this repository for? ###

Geoview makes beautiful maps from spreadsheets.
It color codes the data so you can see where your data is trending.

### How do I get set up? ###

Copy the source.
Login to the Google API Console (https://console.developers.google.com)
Create a project.
Turn on the Geocoding API.
Generate a credential key.
Paste the key into geoview.php.

### Who do I talk to? ###

KarlHenselin
<?php
require("config.php");
if(!$_POST['fileout']){
	header('Location: index.html');
} else{
header('Content-Type: text/html; charset=UTF-8');
echo '<!DOCTYPE html><html lang="en"><head><meta charset="utf-8" /><title>GeoView Mapping Software by Karl Henselin</title>
<link rel="stylesheet" type="text/css" href="geostyle2.css"></head><body class="shaded">';
	$fileout = $_POST['fileout'];
	if ($_FILES["file"]["error"] == 0){
		if (is_uploaded_file($_FILES['uploadedfile']['tmp_name'])){
			$fileName="tempCSV"+rand(100000,999999);
			if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $fileName)) {
				echo '<h2 class="center">Success - Uploaded the file<span class="notdone"> - now processing!</span></h2>';
			} else{
				echo '<h2 class="center">There was an error uploading the file, please try again!</h2>';
			}
		}else{
				die( "That file didn't seem right. Please go back and try again.");
		}
	}else{
		echo "Error: " . $_FILES["file"]["error"] . "<br />";
	}
	$errors = array();
	$wKMLHandle = fopen(getcwd()."/".$fileout.".kml", "wb");
	
	fwrite  ( $wKMLHandle, '<?xml version="1.0" encoding="utf-8"?>');
	fwrite  ( $wKMLHandle, '<kml xmlns="http://earth.google.com/kml/2.0">');
	fwrite  ( $wKMLHandle, '    <Document>');

	$rCSVHandle = fopen("file.csv", "r");
	$keys = fgetcsv( $rCSVHandle , $length);
	$key = htmlspecialchars($keys[5]);
	while (($data = fgetcsv( $rCSVHandle , $length)) !== FALSE){
		//$data[5] is the "year" that we need to get out and store in the ID table of the KML sheet.
		$ids[] = $data[5];
	}
	$ids = array_unique($ids);
	rewind($rCSVHandle);
	$colors = array("ffff0000","ffff0033","ffff0066","ffff0099","ffff00cc", "ffff00ff", "ff9933ff", "ff9966cc", "ff999999", "ff99cc66", "ff99ff33", "ff99ff00", "ff00ff33", "ff00ff66", "ff00ff99", "ff00ffcc", "ff00ffff", "ff00ccff", "ff00cccc", "ff00cc99", "ff00cc66", "ff00cc33", "ff00cc00", "ff009933", "ff009966", "ff009999", "ff0099ff");
	foreach ($ids as $count=>$id){
		fwrite  ( $wKMLHandle, '<Style id="'.$id.'"><IconStyle><color>'.$colors[$count % count($colors)].'</color><colorMode>normal</colorMode><scale>1.0</scale><Icon><href>http://maps.google.com/mapfiles/kml/pal4/icon1.png</href></Icon></IconStyle><LabelStyle><scale>0</scale></LabelStyle></Style>');
	}
	$flag = true;
	while (($data = fgetcsv( $rCSVHandle , $length)) !== FALSE){
		if($flag) { $flag = false; continue; }
		$link = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($data[1])."+".urlencode($data[2])."+".urlencode($data[3])."+".urlencode($data[4]).'&output=csv&key='.$APIKey;
		$test = file_get_contents($link);
		//echo "<br />$test - $link'<br/>";
		$result = json_decode($test);
		if ($result->{"status"} == "OK"){
			$addressData = $result->{'results'}[0]->{"geometry"}->{"location"};
			$lat = $addressData->{"lat"};
			$lng = $addressData->{"lng"};
			//echo "<br />TEST: lat $lat, Lng $lng'<br/>";
			if ($error = outputeach($data, $lat, $lng, $wKMLHandle, $key)){
				array_push($errors, "There was a problem processing " . $error);
			}
		}else{
			array_push($errors, "There was an error processing the address for " . $data[0]);//error for name...
		}
		  @ob_flush();
		  flush();
	}
	echo "<br /><br /><br />";
	if($errors){
		foreach($errors as $error){
			Echo "<br />$error";
		}
	} else {
			echo "No Errors!";
	}
	fwrite  ( $wKMLHandle, '	</Document></kml>');
	fclose ($wKMLHandle);
	fclose ($rCSVHandle);
	unlink($fileName);
	echo '<br /> <a href="download.php?filename='.$fileout.'.kml">Click here to download the file.</a>';
	echo "<br />After downloading and testing the file, click <a href = delete.php?filename=$fileout.kml>here</a> to delete the file.";
}
function outputeach($data, $lat, $lng, $wKMLHandle, $key){
	//print_r($data2);
	if ($year = fouryear($data[5])){
		fwrite  ( $wKMLHandle, '	<Placemark>');
		fwrite  ( $wKMLHandle, '		<name>'.htmlspecialchars($data[0]).'</name>');
		fwrite  ( $wKMLHandle, '		<styleUrl>#'.$year.'</styleUrl>');
		fwrite  ( $wKMLHandle, '		<description>&lt;div&gt;'.htmlspecialchars($data[1]).'&lt;br&gt;'.htmlspecialchars($data[2]).', '. htmlspecialchars($data[3]) . " " . htmlspecialchars($data[4]). "&lt;br&gt; ".$key ." " . $year .'&lt;/div&gt;</description>');
		fwrite  ( $wKMLHandle, '		 <Point><coordinates>'.$lng.",".$lat.",0".'</coordinates></Point>');
		fwrite  ( $wKMLHandle, '	</Placemark>');
	} else {
		return ("the date for " . $data[0]);
	}
}
function fouryear($baddate){
	$baddate = array_pop((explode('/',$baddate)));
	if ($baddate){
		return date("Y", mktime(1,1,1,1,1,$baddate));
	} Else {
		return date("Y", mktime(1,1,1,1,1));
	}
}
?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<?php
echo $analyticsCode;
?>

</body></html>
